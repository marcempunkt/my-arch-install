# My Arch Linux Install Guide

### Table of Content
•[Install Arch](#Install Arch)\
•[Migrate Arch to Parabola](#Migrate Arch Install to Parabola)\
•[Install Xorg & Window Manager](#Install xorg and window manager)\
•[Additional Software](#Additional Software I need)

## Install Arch 

### Set Keyboardlayout

```
$ ls /usr/share/kbd/keymaps/**/*.map.gz | less
or
$ localectl list-keymaps

$ loadkeys de-latin1
```

### Check if UEFI or BIOS

if this directory exists then you are booted into UEFI if not then BIOS\
this guide is only for UEFI biggest difference for BIOS is in partitioning

```
$ ls /sys/firmware/efi/efivars
```

### Connect to the internet with WIFI

```
$ iwctl
```

### Set timezone

```
$ timedatectl set-ntp true
$ timdatectl set-timezone Europe/Berlin
```

### Partitioning

List all available drives/partitions

```
$ fdisk -l
```

Start partitioning 

```
$ fdisk /dev/sda
```

m => for help\
g => create new label with GPT Partition Table  
n => create new partition\
t => set type of partition\
&emsp;ESP should be set to "EFI System"\
&emsp;Boot should be Default or "Linux Filesystem"\
&emsp;Root should be Default or "Linux Filesystem"\
p => see changes that will apply if you write the partition to the disk\
w => write partition table to disk\

EFI System Partition (ESP) should min. 550M\
Boot Partition around min. 2G\
Root as big as you want it to be ;)

### Format your partitions

ESP should be formatted to F32\
while home, root & boot should be formatted to ext4

```
$ mkfs.fat -F 32 /dev/sda1
$ mkfs.ext4 /dev/sda2
$ mkfs.ext4 /dev/sda3
```

### Mount Partitions to the system

```
$ mount /dev/sda3 /mnt
$ mkdir /boot
$ mount /dev/sda2 /mnt/boot
$ mkdir /boot/efi
$ mount /dev/sda1 /mnt/boot/efi
```

### Install the base system

```
$ pacstrap /mnt base linux linux-firmware
```

### Generate fstab File

the command genfstab creates the content for the fstab file

```
$ genfstab -U /mnt >> /mnt/etc/fstab
```

change root into the will be installed system

```
$ arch-chroot /mnt
```

### Create Swapfile

```
$ dd if=/dev/zero of=/swapfile bs=1M count=128896 status=progress
$ chmod 600 /swapfile
$ mkswap /swapfile
$ swapon /swapfile
```

add swapfile to fstab file
```
#<device> <dir> <type> <options> <dump> <fsck>
/swapfile none  swap   defaults  0      0
```

### Set time

ln => creates link between two files\
-f => delete link if it already exists\
-s => create symbolic link instead of hard link

```
$ ln -sf /usr/share/zoneinfo/Europe/Berlin /etc/localtime
```

hwclock is a administrations tool for "time clocks"\
--systohc sets the hardware clock of the system clock and updates the timestamps in /etc/adjtime

```
$ hwclock --systohc
```

### Set Language

uncomment your language(s) in /etc/locale.gen then run

```
$ locale-gen
```

in /etc/locale.conf add:

```
LANG=en_US.UTF-8
LC_TIME=de_DE.UTF-8
```

in /etc/vconsole.conf:

```
KEYMAP=de-latin1
```

add hostname to /etc/hostname\
in /etc/hosts add this section

```
127.0.0.1    localhost
::1          localhost
127.0.1.1    hostname.localdomain    hostname
```

### create user & and set password for user & root

```
$ passwd
$ useradd -m username
$ passwd username
$ usermod -aG wheel,audio,video,optical,storage username
```

install sudo & uncomment %whell

```
$ pacman -S sudo
$ EDITOR=vim visudo
```

## Internet service

```
$ pacman -S networkmanager
$ systemctl enable NetworkManager
```

### Install Grub-bootloader

install grub

```
$ pacman -S grub
$ pacman -S efibootmgr dosfstools os-prober mtools (if doing UEFI)
```

```
$ grub-install
```

if there are any erros try this command instead

```
$ grub-install --efi-directory=/boot/efi --target=x86_64-efi --bootloader-id=grub_uefi --recheck
```

generate grub config file

```
$ grub-mkconfig -o /boot/grub/grub.cfg
```

### Restart the system

```
$ exit
$ umount -l /mnt
$ reboot
```

### Setup ntp deamon for always accurate time

```
$ pacman -S ntp
```

The `/etc/ntp.conf` should look like this: 

```
# Please consider joining the pool:
#
#     http://www.pool.ntp.org/join.html
#
# For additional information see:
# - https://wiki.archlinux.org/index.php/Network_Time_Protocol_daemon
# - http://support.ntp.org/bin/view/Support/GettingStarted
# - the ntp.conf man page

# Associate to Arch's NTP pool
# server 0.arch.pool.ntp.org
# server 1.arch.pool.ntp.org
# server 2.arch.pool.ntp.org
# server 3.arch.pool.ntp.org

# German NTP Pool
server 0.de.pool.ntp.org
server 1.de.pool.ntp.org
server 2.de.pool.ntp.org
server 3.de.pool.ntp.org

# By default, the server allows:
# - all queries from the local host
# - only time queries from remote hosts, protected by rate limiting and kod
restrict default kod limited nomodify nopeer noquery notrap
restrict 127.0.0.1
restrict ::1

# Location of drift file
driftfile /var/lib/ntp/ntp.drift
```

## Install xorg and window manager

```
$ pacman -S xf86-video-intel xorg mesa lib32-mesa
```

in case of system76 Darter Pro 6/tuxedo InfinityBook S 15 Gen 6:

```
$ pacman -S intel-media-driver xorg mesa lib32-mesa
```

For my GTX960 PC install:

```
$ pacman -S nvidia nvidia-utils lib32-nvidia-utils xorg mesa lib32-mesa libva-mesa-driver mesa-vdpau
```

For Radeon RX 6700 XT install:

```
$ pacman -S xf86-video-amdgpu mesa lib32-mesa vulkan-radeon lib32-vulkan-radeon
```

```
$ pacman -S nitrogen bswpm sxhkd alacritty rofi polybar
```

install & enable lightdm

```
$ pacman -S lightdm lightdm-gtk-greeter
$ systemctl enable lightdm
```

## Install aur helper

```
$ pacman -S --needed base-devel git
$ git clone https://aur.archlinux.org/paru.git
$ cd paru
$ makepkg -si
```

with paru install the rest of the packages needed for my arch desktop

```
caffeine-ng 
picom-jonaburg-git 
protonmail-bridge 
timeshift 
icecat-bin 
librewolf-bin 
minecraft-launcher
```

## Additional Software I need

Chinese, Korean & Japanese Input

```
fcitx5
fcitx5-chewing
fctix5-configtool
fcitx5-gtk
fctix5-hangul 
fcitx5-mozc 
fcitx5-chewing 
fcitx5-nord
fcitx5-qt
```

Edit /etc/environment to:

```
#
# This file is parsed by pam_env module
#
# Syntax: simple "KEY=VAL" pairs on separate lines
#
GTK_IM_MODULE=fcitx
QT_IM_MODULE=fcitx
XMODIFIERS=@im=fcitx
```

Indicators

```
networkmanager network-manager-applet redshift volumeicon 
```

Blutetooth

```
bluez bluez-tools blueberry bluez-cups bluez-hid2hci bluez-libs bluez-utils 
```

Audio TODO pipewire

```
pulseaudio pulseaudio-bluetooth hidapi alsa-utils pulseaudio pavucontrol
```

Some Software I use:

```
$ pacman -S steam lutris wine-staging winetricks gnome-screenshot thunderbird obs-studio anki gnome-clocks file-roller telegram-desktop chromium gnome-boxes cheese simplescreenrecorder nemo lxappearance-gtk3 gimp hexchat inkscape kdenlive gnome-calculator gnome-calendar gnome-disk-utility simple-scan evince gnome-font-viewer calibre krita qbittorrent libreoffice-fresh vlc gedit totem emacs
```

Theme:

```
$ pacman -S gnome-themes-extras adwaita-icons
```

### Download PIA Client

Download the install script

https://deu.privateinternetaccess.com/installer/x/download_installer_linux

```
$ sh pia-linux-.....run
```

### Create Home folders

```
$ pacman -S xdg-user-dirs
$ xdg-user-dirs-update
```

### Dotfiles

```
git clone --bare https://www.gitlab.com/username/dotfiles.git $HOME/Documents/Projects/dotfiles.git
alias "dots"="/usr/bin/git --git-dir=$HOME/Documents/Projects/dotfiles.git/ --work-tree=$HOME"
dots checkout
dots config --local status.showUntrackedFiles no
```
