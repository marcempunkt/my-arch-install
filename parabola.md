## Migrate Arch Install to Parabola 

edit this line

```
$ vim /etc/pacman.conf

# RemoteFileSigLevel = Required
```
to
```
RemoteFileSigLevel = Never
```

Installing the Parabola keyring & mirror list package from the internet

```
$ pacman -U https://parabola.nu/packages/core/i686/archlinux32-keyring-transition/download/
$ pacman -U https://parabola.nu/packages/libre/x86_64/parabola-keyring/download/
$ pacman -U https://parabola.nu/packages/libre/x86_64/pacman-mirrorlist/download/
```

change /etc/pacman.conf back

```
$ vim /etc/pacman.conf

RemoteFileSigLevel = Required DatabaseOptional
```

Edit /etc/pacman.conf\
add libre & nonprism repo

```
[libre]
Include = /etc/pacman.d/mirrorlist

....

[nonprism]
Include = /etc/pacman.d/mirrorlist
```

Clear the pacman cache

```
$ pacman -Scc
```

inisde the /etc/pacman.d/ directory are two files\
&emsp;mirrorlist&emsp;Is the arch mirrorlist\
&emsp;mirrorlist.pacnew&emsp;new mirrorlist\
replace the old mirrorlist with the new

```
$ cp /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.arch
$ cp /etc/pacman.d/mirrorlist.pacnew /etc/pacman.d/mirrorlist
```

Sincing the repository & update keyring

```
$ pacman -Syy
$ pacman-key --refresh
```

Upgrade (additionally downgrade) packages of your system

```
$ pacman -Suu pacman your-freedom your-privacy
```

reinstall grub config

```
$ grub-mkconfig -o /boot/grub/grub.cfg
```
