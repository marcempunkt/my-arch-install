### Installing Printer support (mainly for hp printer)

Install cups

```
$ pacman -S avahi cups cups-openrc cups-pdf
```

foomatic for more printer support but not necessary for hp printer

```
$ pacman -S foomatic-db-engine foomatic-db foomatic-db-ppds foomatic-db-nonfree foomatic-db-nonfree-ppds
```

Install hp driver

```
$ pacman -S hplip
```

Enable avahi-deamon and install nss-mdns 

```
$ pacman -S nss-mdns
```

Edit /etc/nsswitch.conf and ahcnge the hosts line to:

```
hosts: ... mdns_minimal [NOTFOUND=return] resolve [!UNAVAIL=return] dns ...
```

Install GUI for adding Printers

```
$ pacman -S system-config-printer
```

And voilá you can print with hp officejet 4720 series
