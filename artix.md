### Install base system

```
$ basestrap /mnt base base-devel openrc elogind-openrc
$ basestrap /mnt linux linux-firmware
```

### Use fstabgen to generate /etc/fstab

```
$ fstabgen -U /mnt >> /mnt/etc/fstab
```

Incase of permission problems run:

```
$ fstabgen -U /mnt | sudo tee /mnt/etc/fstab
```

or

```
$ sudo bash -c 'fstabgen -U /mnt >> /mnt/etc/fstab'
```



### change root

```
$ artix-chroot /mnt
```

### hostname

Edit /etc/conf.d/hostname:

```
hostname='myhostname'
```

### dm-crypt

```
$ pacman -S cryptsetup cryptsetup-openrc
```

The functionality of /etc/crypttab is inside /etc/conf.d/dmcrypt  
Edit /etc/conf.d/dmcrypt file to something like this:

```
## luks_home
target=luks_home
source=UUID="uuid"
remdev=/dev/mapper/luks_home
```

Add dmcrypt to the boot process

```
$ rc-update add dmcrypt sysinit 
```

### Enable Arch Repos

Do this:

```
$ pacman -S artix-archlinux-support
$ pacman-key --populate archlinux
```

Then add this to the end of /etc/pacman.conf:

```
## ARCHLINUX

#[testing]
#Include = /etc/pacman.d/mirrorlist-arch

[extra]
Include = /etc/pacman.d/mirrorlist-arch

#[community-testing]
#Include = /etc/pacman.d/mirrorlist-arch

[community]
Include = /etc/pacman.d/mirrorlist-arch

#[multilib-testing]
#Include = /etc/pacman.d/mirrorlist-arch

#[multilib]
#Include = /etc/pacman.d/mirrorlist-arch
```

Update repositories:

```
$ pacman -Syy
```
