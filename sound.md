### SOUND

lib32-libpulse lib32-alsa-plugins

Install the pulseaudio package.

Some PulseAudio modules are not included in the main package and must be installed separately if needed:

    pulseaudio-alsa for PulseAudio to manage ALSA as well, see #ALSA
    pulseaudio-bluetooth for bluetooth support (Bluez), see bluetooth headset page
    pulseaudio-equalizer for equalizer sink (qpaeq)
    pulseaudio-jack for JACK sink, source and jackdbus detection
    pulseaudio-lirc for infrared volume control with LIRC
    pulseaudio-zeroconf for Zeroconf (Avahi/DNS-SD) support
