### LUKS Encryption

load dm-crypt and dm-mod kernel module

```
$ modprobe dm-crypt
$ modprobe dm-mod
```

encrypt a partition

```
$ cryptsetup luksFormat -v -s 512 -h sha512 /dev/sda3
```

open encrypted partition

```
$ cryptsetup open /dev/sda3 luks_root
```

for encrypted home partition

```
$ cryptsetup open /dev/sda4 luks_home
```

make filesystem to the encrypted drives

```
$ mkfs.ext4 /dev/mapper/luks_root
```

mount encrypted partition

```
$ mount /dev/mapper/luks_root /mnt
```

do this steps after installing grub!\
edit /etc/default/grub and change GRUB_CMDLINE_LINUX to this:

```
GRUB_CMDLINE_LINUX="cryptdevice=/dev/sdaX:luks_root"
# or better
GRUB_CMDLINE_LINUX="cryptdevice=UUID=<UUID of your root partition>:luks_root"
```

edit /etc/mkinitcpio.conf HOOKS section and add "block encrypt"

```
HOOKS=(base udev ... block encrypt ...)
```

run

```
$ mkinitcpio -p linux
```

get uuid of encrypted home partition

```
$ cryptsetup luksUUID /dev/sdaX
```

add encrypted home partition to crypttab\
crypttab is read before fstab, so that dm-crypt containers can be unlocked before the file system inside is mounted.

```
$ vim /etc/crypttab

#home
luks_home    /dev/disk/by-uuid/UUID-COMES-HERE     none    luks
```

the first field has to be the name of the encrypted partition because it will get mounted to /dev/mapper/nameofthepartition\
the second field is the path to the drive\
the third field is the password or keyfile\
the fourth/last field is the encryption method

